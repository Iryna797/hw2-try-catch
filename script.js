const books = [
    {
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70
    },
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    },
    {
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    },
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];
  
// 1й варіант

// const root = document.querySelector("#root");
// let list = ``;

//   // перевіряємо на наявність властивостей в об'єктах,
// const listValid = books.forEach(book => {
//   try {
//     if (!book.name) {
//       throw new Error(`name is undefined`)
//     }
//     if (!book.author) {
//       throw new Error(`author is undefined`)
//     }
//     if (!book.price) {
//       throw new Error(`price is undefined`)
//     }
//    // якщо всі властивості є то додаємо їх у вигляді li-списку на сторінку.
//     list += `<li> Автор: ${book.author}, Назва книги: "${book.name}"<br>Ціна: ${book.price}</li>`
//   } catch (error) {
//     console.log(`${error.name}: ${error.message}`);
//   }
// });

// const ul = document.createElement('ul');
// ul.insertAdjacentHTML('afterbegin', list);
// root.append(ul);


//за бажанням
// console.log(root);

// 2й варіант

function validateBook(book) {
  if (!book.name) {
    throw new Error(`name is undefined`)
  }
  if (!book.author) {
    throw new Error(`author is undefined`)
  }
  if (!book.price) {
    throw new Error(`price is undefined`)
  }
}

function renderList(books) {
  let list = ``;
  books.forEach(book => {
    try {
      validateBook(book);
      list += `<li> Автор: ${book.author}, Назва книги: "${book.name}"<br>Ціна: ${book.price}</li>`
    } catch (error) {
      console.log(`${error.name}: ${error.message}`);
    }
  });
  
  const ul = document.createElement('ul');
  ul.insertAdjacentHTML('afterbegin', list);
  return ul;
}

const root = document.querySelector("#root");
root.append(renderList(books));